# coding: utf-8

from django.conf.urls import url
from views import IndexView, ArticleView, TeaserView, ArticleRedirectView, ContactFormView, TemplateView, CategorieView


app_name = 'website'
urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^ajax/contact/$', ContactFormView.as_view(), name='contact'),
    url(r'^article/(?P<pk>[0-9]+)/$', ArticleRedirectView.as_view(), name='article-redirect'),
    url(r'^article/(?P<slug>[-\w]+)/$', ArticleRedirectView.as_view(), name='article-redirect'),
    url(r'^article/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$', ArticleView.as_view(), name='article'),
    url(r'^categories/(?P<pk>[0-9]+)/(?P<slug>[-\w]+)/$', CategorieView.as_view(), name='categories'),
    url(r'^client9/$', TemplateView.as_view(template_name="clients/client1.html"), name="client1"),
    url(r'^client2/$', TemplateView.as_view(template_name="clients/client2.html"), name="client2"),
    url(r'^client3/$', TemplateView.as_view(template_name="clients/client3.html"), name="client3"),
    url(r'^client4/$', TemplateView.as_view(template_name="clients/client4.html"), name="client4"),
    url(r'^client5/$', TemplateView.as_view(template_name="clients/client5.html"), name="client5"),
    url(r'^client6/$', TemplateView.as_view(template_name="clients/client6.html"), name="client6"),
    url(r'^client7/$', TemplateView.as_view(template_name="clients/client7.html"), name="client7"),
    url(r'^client8/$', TemplateView.as_view(template_name="clients/client8.html"), name="client8"),
    url(r'^client9/$', TemplateView.as_view(template_name="clients/client9.html"), name="client9"),
    url(r'^teaser$', TeaserView.as_view(), name='launch'),
]
