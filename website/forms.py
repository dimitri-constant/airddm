# coding: utf-8

from django import forms
from localflavor.fr.forms import FRPhoneNumberField
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class ContactForm(forms.Form):
    full_name = forms.CharField(label='Your full name *', max_length=100, required=True)
    email = forms.EmailField(label='Your email *', max_length=100, required=True)
    company = forms.CharField(label='Your company *', max_length=100, required=True)
    phone_number = FRPhoneNumberField(label='Your phone number *', required=True)
    role = forms.CharField(label='Your role *', max_length=100, required=True)
    message = forms.CharField(label='Your message *', widget=forms.Textarea, required=True)


class CurriculumForm(forms.Form):
    first_name = forms.CharField(label='Your first name *', max_length=100, required=True)
    last_name = forms.CharField(label='Your last name *', max_length=100, required=True)
    email_cv = forms.EmailField(label='Your email *', max_length=100, required=True)
    phone_number_cv = FRPhoneNumberField(label='Your phone number *', required=True)
    curriculum = forms.FileField(label='Your curriculum vitae *')

    def clean_curriculum(self):
        curriculum = self.cleaned_data['curriculum']
        curriculum_type = curriculum.content_type.split('/')[1]
        print curriculum_type.lower()
        if curriculum_type.lower() in settings.CONTENT_TYPES:
            if curriculum._size > settings.MAX_UPLOAD_SIZE:
                raise forms.ValidationError(_(u'La taille de votre fichier ne doit pas dépasser %s. Taille actuelle : %s ') % (filesizeformat(settings.MAX_UPLOAD_SIZE), filesizeformat(curriculum._size)))
        else:
            raise forms.ValidationError(_(u'Extension de fichier non supportée'))
        return curriculum
