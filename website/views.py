# coding: utf-8

from django.views.generic import TemplateView, DetailView, RedirectView, FormView, ListView
from django.views.generic.edit import FormMixin
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.contrib import messages

from forms import ContactForm, CurriculumForm
from models import Article, Categorie

from website.emails import contact_us, contact_us_copy, curriculum_cv, curriculum_cv_copy


# Create your views here.
class ContactFormView(FormView):
    form_class = ContactForm
    success_message = "Nous avons bien pris en compte votre message et nous vous répondrons dans les plus brefs délais."
    success_url = "/#contact"

    def form_valid(self, form):
        # We make sure to call the parent's form_valid() method because
        # it might do some processing (in the case of CreateView, it will
        # call form.save() for example).
        full_name = form.cleaned_data['full_name']
        email = form.cleaned_data['email']
        company = form.cleaned_data['company']
        role = form.cleaned_data['role']
        message = form.cleaned_data['message']
        phone_number = form.cleaned_data['phone_number']
        contact_us(full_name, email, company, role, phone_number, message)
        contact_us_copy(email)
        response = HttpResponseRedirect(self.get_success_url())
        if self.request.is_ajax():
            data = {
                'success_message': self.success_message,
            }
            return JsonResponse(data)
        else:
            return response

    def form_invalid(self, form):
        response = HttpResponseRedirect(self.get_success_url())
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response


class IndexView(FormMixin, ListView):
    '''
    HomePage
    '''
    model = Article
    template_name = 'website/homepage/index.html'
    queryset = Article.objects.all().order_by('id')
    success_url = "/#contact"
    form_class = CurriculumForm

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, 'candidature validée')
        return self.success_url

    def form_valid(self, form):
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        email_cv = form.cleaned_data['email_cv']
        phone_number_cv = form.cleaned_data['phone_number_cv']
        curriculum = self.request.FILES['curriculum']
        curriculum_cv(first_name, last_name, email_cv, phone_number_cv, curriculum)
        curriculum_cv_copy(email_cv)
        return super(IndexView, self).form_valid(form)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        super(IndexView, self).get(request, *args, **kwargs)
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['first_form'] = ContactForm()
        context['snd_form'] = CurriculumForm()
        return context


class ArticleRedirectView(RedirectView):
    model = Article

    def get_redirect_url(self, *args, **kwargs):
        if 'pk' in kwargs.keys():
            article = get_object_or_404(Article, pk=kwargs['pk'])
        else:
            article = get_object_or_404(Article, slug=kwargs['slug'])
        return reverse('website:article', kwargs={'pk': article.pk, 'slug': article.slug})


class ArticleView(DetailView):
    model = Article
    template_name = 'website/blog/article.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ArticleView, self).get_context_data(*args, **kwargs)
        context['articles'] = Article.objects.all()
        context['categorie'] = Categorie.objects.all()
        return context


class TeaserView(TemplateView):
    '''
    Teaser
    '''
    template_name = 'website/teaser/index.html'


class CategorieView(DetailView):
    model = Categorie
    template_name = 'website/blog/categorie.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CategorieView, self).get_context_data(*args, **kwargs)
        current_category = context['categorie']
        context['current_articles'] = current_category.article_set.all()
        context['articles'] = Article.objects.all()
        context['all_categorie'] = Categorie.objects.all()
        return context
