from django.contrib import admin

from .models import Article, Categorie

# Register your models here.


class CategorieAdmin(admin.ModelAdmin):
    fields = ['categories_text', 'short_description']
    list_display = ('categories_text', 'short_description')
    list_filter = ['categories_text']
    exclude = ('slug',)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'pub_date', 'slug')
    exclude = ('slug',)

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super(ArticleAdmin, self).save_model(request, obj, form, change)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Categorie, CategorieAdmin)
