# coding: utf-8
from PIL import Image
import os
import shutil

from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

from django.conf import settings

# Create your models here.


class Categorie(models.Model):
    categories_text = models.CharField(max_length=200)
    short_description = models.CharField(max_length=200, blank=True)
    slug = models.SlugField(max_length=200)

    def __str__(self):
        return self.categories_text

    class Meta:
        verbose_name = ('Catégorie')
        verbose_name_plural = ('Catégories')

    def save(self):
        self.slug = slugify(self.categories_text)
        super(Categorie, self).save()


def upload_path(obj, filename):
    extension = filename[filename.rfind("."):]
    filename = filename[:filename.find(extension)]
    return 'illustration/{0}{1}'.format(slugify(filename), extension)


class Article(models.Model):
    categories = models.ManyToManyField(Categorie, blank=False)
    title = models.CharField(max_length=200, blank=False)
    subtitle = models.CharField(max_length=200, blank=False)
    text = RichTextUploadingField("Article Text", blank=False)
    img_illustration = models.ImageField(upload_to=upload_path, default='illustration/no_image.png')
    pub_date = models.DateTimeField('date published', blank=False)
    slug = models.SlugField(max_length=200)
    author = models.ForeignKey(User)

    def get_or_create_thumbnail(self):
        path = settings.MEDIA_ROOT + 'illustration/'
        img_name = self.img_illustration.name
        extension = img_name[img_name.rfind("."):]
        img_name = img_name[:img_name.find(extension)]
        filename = img_name[img_name.rfind("/") + 1:]
        if not os.path.exists(path + filename + "_thumbnail.path"):
            size = 400, 300
            thumbnail = Image.open(self.img_illustration)
            thumbnail.thumbnail(size)
            thumbnail.save(path + filename + "_thumbnail" + extension)
            thumbnail_path = path + filename + "_thumbnail" + extension
            return thumbnail_path

#    def move_img_illustration(self):
#       if not os.path.exists(settings.MEDIA_ROOT + 'illustration/{0}'.format(str(self.pk))):
#          os.mkdir(settings.MEDIA_ROOT + 'illustration/{0}'.format(str(self.pk)))
#       img_name = self.img_illustration.name
#       img_path = self.img_illustration.path
#       if not os.path.isfile(img_name):
#           img_name = img_name[img_name.rfind("/") + 1:]
#           new_path = settings.MEDIA_ROOT + 'illustration/{0}/{1}'.format(str(self.pk), img_name)
#           shutil.move(img_path, new_path)
#           self.img_illustration = new_path
#           self.save()
#       else:
#           return True

    def save(self):
        self.slug = slugify(self.title)
        super(Article, self).save()
        self.get_or_create_thumbnail()
        # self.move_img_illustration()

    class Meta:
        verbose_name = ('Article')
        verbose_name_plural = ('Articles')
