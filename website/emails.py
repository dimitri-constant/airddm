# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


def contact_us(full_name, email, company, role, phone_number, message):
    """ Send the email with provided data """
    subject, from_email, to = 'Prise de contact AIRDDM', email, 'dconstant@airddm.com'
    text_content = render_to_string('website/emails/contact_us.html', {'full_name': full_name, 'email': email, 'company': company, 'role': role, 'phone_number': phone_number, 'message': message})
    html_content = render_to_string('website/emails/contact_us.html', {'full_name': full_name, 'email': email, 'company': company, 'role': role, 'phone_number': phone_number, 'message': message})
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def contact_us_copy(email):
    """ Send the email with provided data """
    subject, from_email, to = 'Confirmation de votre prise de contact AIRDDM', 'contact@airddm.com', email
    text_content = render_to_string('website/emails/contact_us_copy.html', )
    html_content = render_to_string('website/emails/contact_us_copy.html', )
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def curriculum_cv(first_name, last_name, email_cv, phone_number_cv, curriculum):
    """ Send the email with provided data """
    subject, from_email, to = 'Réception de candidature sur AIRDDM', email_cv, 'dconstant@airddm.com'
    text_content = render_to_string('website/emails/curriculum.html', {'first_name': first_name, 'last_name': last_name, 'email': email_cv, 'phone_number': phone_number_cv, 'curriculum': curriculum})
    html_content = render_to_string('website/emails/curriculum.html', {'first_name': first_name, 'last_name': last_name, 'email': email_cv, 'phone_number': phone_number_cv, 'curriculum': curriculum})
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.attach(curriculum.name, curriculum.read(), curriculum.content_type)
    msg.send()


def curriculum_cv_copy(email_cv):
    """ Send the email with provided data """
    subject, from_email, to = 'Confirmation de votre candidature AIRDDM', 'contact@airddm.com', email_cv
    text_content = render_to_string('website/emails/curriculum_copy.html', )
    html_content = render_to_string('website/emails/curriculum_copy.html', )
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()
