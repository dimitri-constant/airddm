<?php
function send_email($name, $email, $company, $phone, $role, $message) {
    $subject = "Prise de contact";
    $to = "contact@airddm.com";
    $message_email = "Nom: " . $name .
        "\r\nEntreprise: " . $company .
        "\r\nEmail: " . $email .
        "\r\nTéléphone: " . $phone;
    if ($role != "") {
        $message_email .= "\r\nPoste: " . $role;
    }
    if ($message != "") {
        $message_email .= "\r\n\r\nMessage:\r\n" . $message;
    }
    $response = mail($to, $subject, $message_email);
    if ($response) {
        $response_array['status'] = 'success';
        echo json_encode($response_array);
    } else {
        $response_array['status'] = 'error';
        echo json_encode($response_array);
    }
}

function subscribe_newsletter($email) {
    $subject = "Inscription à la newsletter";
    $to = "newsletter@airddm.com";
    $message = "L'utilisateur s'est inscrit à la newsletter :\r\n" .
        "Email : " . $email;
    $response = mail($to, $subject, $message);
    if ($response) {
        $response_array['status'] = 'success';
        echo json_encode($response_array);
    } else {
        $response_array['status'] = 'error';
        echo json_encode($response_array);
    }
}

if(isset($_POST['action']) && !empty($_POST['action'])) {
    $action = $_POST['action'];
    switch($action) {
        case 'send_email' :
            send_email($_POST['name'], $_POST['email'], $_POST['company'], $_POST['phone'], $_POST['role'], $_POST['message']);
            break;
        case 'subscribe_newsletter' :
            subscribe_newsletter($_POST['email']);
            break;
        default:
            break;
    }
}

?>